#include<stdio.h>
#include "MBDevice.h"
#define REG_MAIN_ADDR 0x10
#define REG_AUX_ADDR  0x11
#define GPIO_ADDR     0x12
#define ON 1
#define OFF 0   

MBSlave regulator_aux(REG_AUX_ADDR);
MBSlave regulator_main(REG_MAIN_ADDR);
MBSlave gpio(GPIO_ADDR);
struct reg_main_control_t
{
  union
  {
    struct
    {
      uint16_t pwr_pc0 :1;
      uint16_t pwr_pc1 :1;
      uint16_t pwr_sonar :1;
      uint16_t pwr_altimeter :1;
      uint16_t pwr_dsp :1;
      uint16_t pwr_dvl :1;
      uint16_t pwr_lna :1;
      uint16_t pwr_imu :1;
      uint16_t pwr_hub :1;
      uint16_t pwr_fog_imu :1;
      uint16_t pwr_lan :1;
      uint16_t pwr_spare :1;
      uint16_t unused :4;
    };

    uint16_t rawData;
  };
};

struct reg_aux_control_t
{
  union
  {
    struct
    {
      uint16_t pwr_m0 :1;
      uint16_t pwr_m1 :1;
      uint16_t pwr_m2 :1;
      uint16_t pwr_m3 :1;
      uint16_t pwr_m4 :1;
      uint16_t pwr_m5 :1;
      uint16_t pwr_m6 :1;
      uint16_t pwr_m7 :1;
      uint16_t pwr_pneumatic :1;
      uint16_t pwr_spare :1;
      uint16_t pwr_main :1;
      uint16_t pwr_gpio :1;
      uint16_t pwr_t_logic :1;
      uint16_t pwr_lamp1 :1;
      uint16_t pwr_lamp2 :1;ROS_INFO("Ready to add two ints.");
      uint16_t unused :1;
    };

    uint16_t rawData;
  };
};

struct gpio_control_t
{
  union
  {
    struct
    {
      uint16_t sink_a :1;
      uint16_t sink_b :1;
      uint16_t sink_c :1;
      uint16_t sink_d :1;
      uint16_t sink_e :1;
      uint16_t sink_f :1;
      uint16_t sink_g :1;
      uint16_t led0 :1;
      uint16_t led1 :1;
      uint16_t led2 :1;
      uint16_t led3 :1;
      uint16_t led4 :1;
      uint16_t unused :4;
    };
    uint16_t rawData;
  };
};
reg_main_control_t reg_main_control;
reg_aux_control_t reg_aux_control;
gpio_control_t gpio_control;

/* Interactive test
 int Prompt()
 {
 enum{st_space, st_word, st_end};
 int i =0, j=0, state=st_word, count, val, ret;
 char c;
 char inp[32][16];
 printf (">> ");
 while(1)
 {
 if (state == st_end)
 {
 inp[i][j] = '\0';
 break;
 }
 c = getchar();
 if (c == '\n')
 {
 inp[i][j] = '\0';
 break;
 }
 switch(state)
 {
 case st_space:
 if (c==' ') state = st_space;
 else
 {
 inp[i][j] = '\0';
 i++;
 j=0;
 inp[i][j] = c;
 j++;
 state = st_word;
 }
 break;
 case st_word:
 if (c==' ') state = st_space;
 else
 {
 inp[i][j] = c;
 j++;
 }
 break;
 default:
 state = st_end;
 break;
 }
 }
 count = i+1;
 //for (i=0;i<count;i++)   printf("%s\n",inp[i]);

 if (strcmp(inp[0],"actm") == 0)
 {
 printf("activating\n");
 for (i=1;i<count;i++)
 {
 sscanf(inp[i], "%d", &val);
 if (val<0 || val > 15) continue;
 reg_main_control.rawData |= (1<<val);
 }
 ret = regulator_main.Write_multiple_registers(0, 1, &(reg_main_control.rawData) );
 if (ret<=0) printf("FAILED\n");

 }else if (strcmp(inp[0],"deactm") == 0)
 {
 printf("deactivating\n");
 for (i=1;i<count;i++)
 {
 sscanf(inp[i], "%d", &val);
 if (val<0 || val > 15) continue;
 reg_main_control.rawData &= ~(1<<val);
 }
 ret = regulator_main.Write_multiple_registers(0, 1, &(reg_main_control.rawData) );
 if (ret<=0) printf("FAILED\n");
 }else if (strcmp(inp[0],"acta") == 0)
 {
 printf("activating\n");
 for (i=1;i<count;i++)
 {
 sscanf(inp[i], "%d", &val);
 if (val<0 || val > 15) continue;
 reg_aux_control.rawData |= (1<<val);
 }
 ret = regulator_aux.Write_multiple_registers(0, 1, &(reg_aux_control.rawData) );
 if (ret<=0) printf("FAILED\n");

 }else if (strcmp(inp[0],"dim") == 0)
 {
 printf("dimming\n");
 uint16_t dim_val;
 sscanf(inp[1], "%d", &val);
 dim_val = (uint16_t)val;
 if (val<0 || val > 255) return 1;
 ret = regulator_aux.Write_multiple_registers(1, 1, &(dim_val) );
 ret = regulator_aux.Write_multiple_registers(2, 1, &(dim_val) );
 if (ret<=0) printf("FAILED\n");

 }else if (strcmp(inp[0],"deacta") == 0)
 {
 printf("deactivating\n");
 for (i=1;i<count;i++)
 {
 sscanf(inp[i], "%d", &val);
 if (val<0 || val > 15) continue;
 reg_aux_control.rawData &= ~(1<<val);
 }
 ret = regulator_aux.Write_multiple_registers(0, 1, &(reg_aux_control.rawData) );
 if (ret<=0) printf("FAILED\n");
 }else if (strcmp(inp[0],"exit") == 0)
 {
 return 0;
 }else{
 printf("invalid command\n");
 }

 return 1;
 }
 */

int main()
{
  int ret;
  Set_portname("/dev/ttyUSB0");
  regulator_aux = MBSlave(REG_AUX_ADDR);
  regulator_main = MBSlave(REG_MAIN_ADDR);
  gpio = MBSlave(GPIO_ADDR);

  /*example */
  gpio_control.rawData = 0xffff;
  ret = gpio.Write_multiple_registers(0, 1, &(gpio_control.rawData));
  /*--------*/

  Close_port();
  printf("\nTOTAL REQUEST COUNT:%d\n", total_request_count);
  printf("SUCCESS REQUEST COUNT:%d\n", request_success_count);
  printf("LRC_CORRECT COUNT:%d\n", lrc_correct_count);
  printf("LRC_INCORRECT COUNT:%d\n", lrc_incorrect_count);
  return 0;
}

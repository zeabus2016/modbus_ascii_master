#include<stdio.h>
#include "MBDevice.h"
#include <math.h>
#define BMU0_ADDR     0x20
#define ON 1
#define OFF 0   

MBSlave bmu0(BMU0_ADDR, "/dev/ttyUSB0", "LINK1", B1000000, 7, 0, 2);
int main()
{
  int ready = (bmu0.Init() == 0);

  int fd;
  
  printf ("ready:%d\n",ready);
  show_debug_info = 1;
  int ret;
  uint8_t tmp[1];
  uint16_t read_val[4];
	//Set_portname("/dev/ttyUSB0");
  //bmu0 = MBSlave(BMU0_ADDR);
  timeval before, after;
  double t_diff;
  while(1)
  {
      //gettimeofday(&before , NULL);
      
      //gettimeofday(&after , NULL);
      //t_diff = time_diff(before , after);

      ret = bmu0.Read_input_registers(0, 4, read_val);
      ret = bmu0.Read_discrete_inputs(0, 8, tmp);
      if (ret > 0)
      {
          printf("stats:%.2x, voltage : %.2lf V, current : %.5lf A, temp : %.2lf, Charge:%d, percentage:%lf\n",tmp[0], (double)read_val[0]*23.6/65535, ((double)read_val[1]-32767) / 32767 * 0.06 / 0.0005, (double)read_val[2]/65535.0*510-273, read_val[3], (double)(read_val[3]-0x29FB)/(0xCFFF-0x29FB)*100.0 );
          fflush(stdout);
           //printf("Read value : %d, voltage : %.0lf mv, pressure %.4lf psig, Roundtrip : %.0lf usec\n",read_val, voltage*1000, pressure, t_diff);
      } else
      {
           printf("Communication problem.\n");
      }
      //usleep(100000);
  }
		
  printf("\nTOTAL REQUEST COUNT:%d\n",total_request_count);
  printf("SUCCESS REQUEST COUNT:%d\n",request_success_count);
  printf("LRC_CORRECT COUNT:%d\n",lrc_correct_count);
  printf("LRC_INCORRECT COUNT:%d\n",lrc_incorrect_count);
	return 0;
  
}

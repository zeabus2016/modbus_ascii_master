#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <termios.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <unistd.h>  /* UNIX standard function definitions */
#include "MBDatalink.h"
#include "MBDevice.h"

int main(){
	Set_portname("/dev/ttyUSB0");
	const uint8_t slave_address = 0x01;
	MBSlave dut(slave_address); 
	
	int i, ret;
	uint16_t read_start_address, read_quantity, write_start_address, write_quantity;

	/* Unit test No.1 :  Coils test */
	/*
	read_start_address = 0;;
	read_quantity =16; 
	uint8_t read_mb_bit_stream[(read_quantity+7)/8];
	ret = dut.Read_coils(read_start_address, read_quantity, read_mb_bit_stream);
	if(ret)
	{
		printf("Read coils success %d bits, read data : ", ret);
		for (i=0;i<(ret+7)/8;i++)
		{
			printf("[%.02x] ",read_mb_bit_stream[i]);
		}
		printf("\n");
	}else{
		printf("Coils read failed\n");
	}

	write_start_address = read_start_address;
	write_quantity = 16;
	uint8_t write_mb_bit_stream[(write_quantity+7)/8];
	write_mb_bit_stream[0] = 0xA7;
	write_mb_bit_stream[1] = 0x71;
	write_mb_bit_stream[2] = 0xFF;
	ret = dut.Write_multiple_coils(write_start_address, write_quantity, write_mb_bit_stream);
	if(ret)
	{
		printf("Write coils success %d bits\n",ret);
	}else{
		printf("Coils write failed\n");
	} 
	ret = dut.Write_single_coil(7, 0);
	ret = dut.Write_single_coil(1, 0);
	ret = dut.Write_single_coil(11, 1);
	ret = dut.Write_single_coil(15, 1);
	
	read_quantity = 16;
	ret = dut.Read_discrete_inputs(read_start_address, read_quantity, read_mb_bit_stream);
	if(ret)
	{
		printf("Read success %d bits, read data : ",ret);
		for (i=0;i<(ret+7)/8;i++)
		{
			printf("[%.02x] ",read_mb_bit_stream[i]);
		}
		printf("\n");
	}else{
		printf("Discrete input read failed\n");
	}

	// End coil test 
	*/
	// Unit test 2 : Holding register test 
	
	read_start_address = 0;
	read_quantity = 20;
	uint16_t read_registers[read_quantity];

	ret = dut.Read_holding_registers(read_start_address, read_quantity, read_registers);
	if(ret)
	{
		printf("Read success %d words, read data : ", ret);
		for (i=0;i<ret;i++)
		{
			printf("[%.04x] ",read_registers[i]);
		}
		printf("\n");
	}else{
		printf("Read failed\n");
	}
    write_start_address = 0;
	write_quantity = 20;

	uint16_t write_registers[write_quantity];
	for (i=0;i<write_quantity;i++)
	{
		write_registers[i] = i;
	}
	/*
	ret = dut.Write_multiple_registers(write_start_address, write_quantity, write_registers);
	if(ret)
	{
		printf("Write success %d words\n",ret);
	}else{
		printf("write failed\n");
	}
	
	ret = dut.Read_input_registers(read_start_address+2, read_quantity-2, read_registers);
	if(ret)
	{
		printf("Read success %d words, read data : ", ret);
		for (i=0;i<ret;i++)
		{
			printf("[%.04x] ",read_registers[i]);
		}
		printf("\n");
	}else{
		printf("Read failed\n");
	}*/

	//ret = dut.Write_single_register(15, 0xFCAA);
	read_start_address = 0;
	read_quantity = 20;

	for (i=0;i<write_quantity;i++)
	{
		write_registers[i] = i+0x8000;
	}
	int k;
	for (k=0;k<100;k++)
	{
		ret = dut.Read_write_multiple_registers(read_start_address, read_quantity, read_registers, write_start_address, write_quantity, write_registers);
		if(ret)
		{
			//printf("Read/Write success %d words, read data : ", ret);
			for (i=0;i<ret;i++)
			{
				//printf("[%.04x] ",read_registers[i]);
			}
			//printf("\n");
		}else{
			printf("Read/Write failed at %d\n", k);
		}
	}
	/*
	ret = dut.Read_holding_registers(read_start_address, read_quantity, read_registers);
	if(ret)
	{
		printf("Read success %d words, read data : ", ret);
		for (i=0;i<ret;i++)
		{
			printf("[%.04x] ",read_registers[i]);
		}
		printf("\n");
	}else{
		printf("Read failed\n");
	}
	*/
	
	Close_port();
	printf("\nTOTAL REQUEST COUNT:%d\n",total_request_count);
	printf("SUCCESS REQUEST COUNT:%d\n",request_success_count);
	printf("LRC_CORRECT COUNT:%d\n",lrc_correct_count);
	printf("LRC_INCORRECT COUNT:%d\n",lrc_incorrect_count);
	return 0;
}
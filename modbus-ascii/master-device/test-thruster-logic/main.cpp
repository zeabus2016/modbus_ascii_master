#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <termios.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <unistd.h>  /* UNIX standard function definitions */
#include "MBDatalink.h"
#include "MBDevice.h"
#define BLDC_CONTROLLER_ADDRESS 0x01

MBSlave BLDC_controller(BLDC_CONTROLLER_ADDRESS); 

int Prompt()
{
	enum{st_space, st_word, st_end};
	int i =0, j=0, state=st_word, count, val, ret;
	char c;
	char inp[16][16];
	printf (">> ");
	while(1)
	{
		if (state == st_end) 
		{
			inp[i][j] = '\0';
			break;
		}
		c = getchar();
		if (c == '\n')
		{
			inp[i][j] = '\0';
			break;
		}
		switch(state)
		{
			case st_space:
				if (c==' ') state = st_space;
				else 
				{
					inp[i][j] = '\0';
					i++;
					j=0;
					inp[i][j] = c;
					j++;
					state = st_word;
				}
				break;
			case st_word:
				if (c==' ') state = st_space;
				else
				{
					inp[i][j] = c;
					j++;
				}
				break;
			default:
				state = st_end;
				break;
		}
	}
	count = i+1;
	//for (i=0;i<count;i++)		printf("%s\n",inp[i]);

	if (strcmp(inp[0],"activate") == 0)
	{
		printf("activating ");
		for (i=1;i<count;i++)
		{
			sscanf(inp[i], "%d", &val);
			if (val<1 || val > 8) continue;
			//printf("%d ", val);
			ret = BLDC_controller.Write_single_coil(val-1, 1);
			if (ret<=0) printf("FAILED\n");
			

		}
		uint8_t tmp = 0;
		BLDC_controller.Read_coils(0, 8, &tmp);
		printf("[%.02x]\n", tmp);
		printf("\n");

	}else if (strcmp(inp[0],"deactivate") == 0)
	{
		printf("deactivating\n");
		for (i=1;i<count;i++)
		{
			sscanf(inp[i], "%d", &val);
			if (val<1 || val > 8) continue;
			//printf("%d ", val);
			ret = BLDC_controller.Write_single_coil(val-1, 0);
			if (ret<=0) printf("FAILED\n");
		}
		uint8_t tmp = 0;
		BLDC_controller.Read_coils(0, 8, &tmp);
		printf("[%.02x]\n", tmp);
		printf("\n");
	}else if (strcmp(inp[0],"speed") == 0)
	{
		int val;
		printf("setting speed\n");
		for (i=1;i<count;i++)
		{
			sscanf(inp[i], "%d", &val);
			//if (val<1 || val > 8) continue;
			//printf("%d ", val);
			ret = BLDC_controller.Write_single_register(i-1, (uint16_t)val);
			if (ret<=0) printf("FAILED\n");
		}
		printf("\n");
		uint16_t retspd[8];
		ret = BLDC_controller.Read_holding_registers(0, 8, retspd);
		for(i=0;i<8;i++) printf("[%.04x]", retspd[i] );
	}else if (strcmp(inp[0],"readspeed") == 0)
	{
		uint16_t sp[8] = {0};
		printf("reading speed\n");
		ret = BLDC_controller.Read_input_registers(0, 8, sp);
		if (ret<=0) printf("FAILED\n");
		for (i=0;i<8;i++) printf (" %d ",(int16_t)sp[i]);
		printf("\n");

	}else if (strcmp(inp[0],"diag") == 0)
	{
		printf("reading diag\n");
	}else{
		printf("invalid command\n");
	}

	return count;
}

int main(){
	Set_portname("/dev/ttyUSB0");
	
	/* BLDC Controller test */
	//int16_t retspd[8];
	//BLDC_controller.Read_holding_registers(0, 8, (uint16_t*)retspd);
	while (1)	Prompt();
	
	
	Close_port();
	printf("\nTOTAL REQUEST COUNT:%d\n",total_request_count);
	printf("SUCCESS REQUEST COUNT:%d\n",request_success_count);
	printf("LRC_CORRECT COUNT:%d\n",lrc_correct_count);
	printf("LRC_INCORRECT COUNT:%d\n",lrc_incorrect_count);
	return 0;
}
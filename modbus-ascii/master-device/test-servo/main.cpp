#include<stdio.h>
#include "MBDevice.h"
#define SERVO_CONTROLLER_ADDR  0x30
#define GPIO_ADDR              0x12
#define REG_AUX_ADDR           0x11
#define CHANNEL_COUNT  8
#define ON 1
#define OFF 0   

MBSlave servo_controller(SERVO_CONTROLLER_ADDR);
MBSlave regulator_aux(REG_AUX_ADDR);
MBSlave gpio(GPIO_ADDR);

struct reg_aux_control_t
{
   union
   {
       struct
       {
          uint16_t pwr_m0           :1;
          uint16_t pwr_m1           :1;
          uint16_t pwr_m2           :1;
          uint16_t pwr_m3           :1;
          uint16_t pwr_m4           :1;
          uint16_t pwr_m5           :1;
          uint16_t pwr_m6           :1;
          uint16_t pwr_m7           :1;
          uint16_t pwr_pneumatic    :1;
          uint16_t pwr_spare        :1;
          uint16_t pwr_main         :1;  // 11
          uint16_t pwr_gpio         :1; 
          uint16_t pwr_t_logic      :1;
          uint16_t pwr_lamp1        :1;
          uint16_t pwr_lamp2        :1;
          uint16_t unused           :1;
       };

       uint16_t rawData;
   };
};

reg_aux_control_t reg_aux_control;

int Prompt()
{
  uint16_t speed[8];
  enum{st_space, st_word, st_end};
  int i =0, j=0, state=st_word, count, val, ret, k;
  char c;
  char inp[32][16];
  printf (">> ");
  while(1)
  {
    if (state == st_end) 
    {
      inp[i][j] = '\0';
      break;
    }
    c = getchar();
    if (c == '\n')
    {
      inp[i][j] = '\0';
      break;
    }
    switch(state)
    {
      case st_space:
        if (c==' ') state = st_space;
        else 
        {
          inp[i][j] = '\0';
          i++;
          j=0;
          inp[i][j] = c;
          j++;
          state = st_word;
        }
        break;
      case st_word:
        if (c==' ') state = st_space;
        else
        {
          inp[i][j] = c;
          j++;
        }
        break;
      default:
        state = st_end;
        break;
    }
  }
  count = i+1;
  //for (i=0;i<count;i++)   printf("%s\n",inp[i]);

  if (strcmp(inp[0],"speed") == 0)
  {
    printf("Set speed :\n");
    sscanf(inp[1], "%d", &val);
    if (val<1000 || val > 2000) return 1;
    for (k=0;k<CHANNEL_COUNT;k++) speed[k] = val; 
    ret = servo_controller.Write_multiple_registers(0, CHANNEL_COUNT, speed );
    printf("ret=%d", ret);
    if (ret<=0) printf("FAILED\n");
  }else if (strcmp(inp[0],"exit") == 0)
  {
    return 0;
  }else{ 
    printf("invalid command\n");
  }

  return 1;
}
int main()
{
  int ret, i, k, m;
  uint16_t speed[8];
  int fd = 0;
  if (Open_port(&fd, "/dev/ttyArduino") == 0) return -1;
  regulator_aux.Init(fd);
  servo_controller.Init(fd);
  gpio.Init(fd);
  reg_aux_control.rawData = 0;
  //reg_aux_control.rawData = 0b011111111; // Turn all thruster driver
  reg_aux_control.pwr_gpio = 1;
  reg_aux_control.pwr_t_logic = 1;
  for (m=0;m<8;m++)
          speed[m] = 1500;
  ret = regulator_aux.Write_multiple_registers(0, 1, &(reg_aux_control.rawData));
  //ret = servo_controller.Write_multiple_registers(0, CHANNEL_COUNT, speed);
  //getchar();
  //usleep(200000);
  //ret = regulator_aux.Write_multiple_registers(0, 1, &(reg_aux_control.rawData));
  //ret = servo_controller.Write_multiple_registers(0, CHANNEL_COUNT, speed );

  uint16_t read_val[1];
  for (k=0;k<0;k++)
  { 
      ret = gpio.Read_input_registers(0, 1, read_val);
    if (ret>0)
    {
      printf ("read ok \n");
    }else{
      printf("Communication problem %d\n ", ret);
    }
  //usleep(10000);
  //getchar();
  }


  for (k=0;k<1000;k++)
  { 
      for (m=0;m<8;m++)
          speed[m] = 1500+(k + m*20)%500;
      //speed[k%8] = (k>=8) ? 1350 : 1650;
      //speed[k%8] = 1500+k;
     // ret = gpio.Read_input_registers(0, 1, read_val);
      ret = servo_controller.Write_multiple_registers(0, CHANNEL_COUNT, speed );
      //ret = servo_controller.Read_holding_registers(0, CHANNEL_COUNT, speed);
    if (ret>0)
    {
      printf ("read_speed : ");
      for (i=0;i<CHANNEL_COUNT;i++)
      {
          printf("[%03d]", speed[i]);
      }
      printf("\n");
    }else{
      printf("Communication problem %d\n ", ret);
    }
  //usleep(10000);
  //getchar();
  }
  while(0)
  {
    if (Prompt()==0) break;
    ret = servo_controller.Read_holding_registers(0, CHANNEL_COUNT, speed);
    if (ret>0)
    {
      printf ("read_speed : ");
      for (i=0;i<CHANNEL_COUNT;i++)
      {
          printf("[%03d]", speed[i]);
      }
      printf("\n");
    }else{
      printf("Communication problem\n");
    }
  }
		
  Close_port(&fd);
  printf("\nTOTAL REQUEST COUNT:%d\n",total_request_count);
  printf("SUCCESS REQUEST COUNT:%d\n",request_success_count);
  printf("LRC_CORRECT COUNT:%d\n",lrc_correct_count);
  printf("LRC_INCORRECT COUNT:%d\n",lrc_incorrect_count);
	return 0;
}

#include<stdio.h>
#include "MBDevice.h"
#include <math.h>
#define REG_MAIN_ADDR 0x10
#define REG_AUX_ADDR  0x11
#define GPIO_ADDR     0x12
#define BMU0_ADDR     0x20
#define ON 1
#define OFF 0   

MBSlave regulator_aux(REG_AUX_ADDR, "", "", B1000000, 7, 0, 2);
MBSlave regulator_main(REG_MAIN_ADDR, "", "", B1000000, 7, 0, 2);
MBSlave gpio(GPIO_ADDR, "", "", B1000000, 7, 0, 2);
MBSlave bmu0(BMU0_ADDR, "", "", B1000000, 7, 0, 2);

struct gpio_control_t
{
   union
   {
       struct
       {
          uint16_t sink_a           :1;
          uint16_t sink_b           :1;
          uint16_t sink_c           :1;
          uint16_t sink_d           :1;
          uint16_t sink_e           :1;
          uint16_t sink_f           :1;
          uint16_t sink_g           :1;
          uint16_t led0             :1;
          uint16_t led1             :1;
          uint16_t led2             :1;
          uint16_t led3             :1;
          uint16_t led4             :1;
          uint16_t unused           :4;         
       };
       uint16_t rawData;
   };
};
gpio_control_t gpio_control;

int main()
{
  int fd = 0;
  if (Open_port(&fd, "/dev/ttyUSB0") != 0) return -1;
  regulator_aux.Init(fd);
  regulator_main.Init(fd);
  gpio.Init(fd);
  bmu0.Init(fd);
  show_debug_info = 1;
  int ret;
  uint8_t read_val;
  gpio_control.rawData = 0x0000;
  double pressure, voltage;
  //ret = bmu0.Write_multiple_registers(0, 1, &(gpio_control.rawData) );
  timeval before, after;
  double t_diff;
  while(1)
  {
      gpio_control.led0 = 1;
      gpio_control.led1 = 1;
      gpio_control.led2 = 1;
      gpio_control.led3 = 1;
      gpio_control.led4 = 1;
      ret = gpio.Read_discrete_inputs(0, 3, &read_val);
      ret = gpio.Write_multiple_registers(0, 1, &(gpio_control.rawData));
      printf("ret = %d",ret);
      if (ret > 0)
      {
        printf("Read value : %d, SW0=%d, SW1=%d, SW2=%d\n ", read_val, read_val & 0b00000001, (read_val >> 1) & 0b00000001, (read_val >> 2) & 0b00000001);
      } else
      {
        printf("Communication problem.\n");
      }
      usleep(50000);
  }
		
  Close_port(&fd);
  printf("\nTOTAL REQUEST COUNT:%d\n",total_request_count);
  printf("SUCCESS REQUEST COUNT:%d\n",request_success_count);
  printf("LRC_CORRECT COUNT:%d\n",lrc_correct_count);
  printf("LRC_INCORRECT COUNT:%d\n",lrc_incorrect_count);
	return 0;
  
}

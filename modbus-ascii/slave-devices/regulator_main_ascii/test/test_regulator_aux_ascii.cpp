#include<stdio.h>
#include "MBDevice.h"
#define REG_AUX_ADDR  0x10  
#define ON 1
#define OFF 0   

struct control_t
{
   union
   {
       struct
       {
          uint16_t m0   :1;
          uint16_t m1   :1;
          uint16_t m2   :1;
          uint16_t m3   :1;
          uint16_t m4   :1;
          uint16_t m5   :1;
          uint16_t m6   :1;
          uint16_t m7   :1;
          uint16_t m8   :1;
          uint16_t m9   :1;
          uint16_t pwr0   :1;
          uint16_t t_logic_en   :1;
          uint16_t gpio_en :1;
          uint16_t unused :3;
       };

       uint16_t rawData;
   };
};
control_t control;

int main()
{
	Set_portname("/dev/ttyUSB0");
	MBSlave regulator_aux(REG_AUX_ADDR);	
	control.m0 = ON;
	control.m1 = ON;
	control.m2 = ON;
	control.m3 = ON;
	control.m4 = ON;
	control.m5 = ON;
	control.m6 = ON;
	control.m7 = ON;
	control.m8 = ON;
	control.m9 = ON;
	control.pwr0 = ON;
	control.t_logic_en = OFF;
	control.gpio_en = OFF;
  uint16_t read_reg;
	int ret_status = regulator_aux.Read_holding_registers(0, 1, &read_reg );   // start = 0, quantity = 1, 
  ret_status = regulator_aux.Write_multiple_registers(0, 1, &(control.rawData) );
  ret_status = regulator_aux.Read_holding_registers(0, 1, &(control.rawData) );
		
	return 0;
}

#include <MBProtocol.h>
#include <MBDatalink.h>
#include <MBDatatypes.h>
#include <MBSerialMega328.h>
#include <MBDevice.h>

/* Regulator Main */


#define DEV_ADDR   0x10
#define RC_A       A0
#define RC_B       A1
#define RC_C       A2
#define RC_D       A3
#define RC_E       A4
#define RC_F       A5
#define RC_G       3
#define RC_H       4
#define RC_I       5
#define RC_J       6
#define RC_K       7
#define RC_L       8
#define LED_PIN    9
#define D11        11
#define D12        12
#define D13        13
#define DIR_PIN    2 

#define PWR_PC0        RC_A
#define PWR_PC1        RC_B
#define PWR_SONAR      RC_C
#define PWR_ALTIMETER  RC_D
#define PWR_DSP        RC_E
#define PWR_DVL        RC_F
#define PWR_LNA        RC_G
#define PWR_IMU        RC_H
#define PWR_HUB        RC_I
#define PWR_FOG_IMU    RC_J
#define PWR_LAN        RC_K
#define PWR_SPARE      RC_L

struct reg_main_control_t
{
   union
   {
       struct
       {
          uint16_t pwr_pc0           :1;
          uint16_t pwr_pc1           :1;
          uint16_t pwr_sonar         :1;
          uint16_t pwr_altimeter     :1;
          uint16_t pwr_dsp           :1;
          uint16_t pwr_dvl           :1;
          uint16_t pwr_lna           :1;
          uint16_t pwr_imu           :1;
          uint16_t pwr_hub           :1;
          uint16_t pwr_fog_imu       :1;
          uint16_t pwr_lan           :1;
          uint16_t pwr_spare         :1;
          uint16_t unused            :4;
       };

       uint16_t rawData;
   };
};
reg_main_control_t control;

void setup() {
  pinMode(PWR_PC0, OUTPUT);
  pinMode(PWR_PC1, OUTPUT);
  pinMode(PWR_SONAR, OUTPUT);
  pinMode(PWR_ALTIMETER, OUTPUT);
  pinMode(PWR_DSP, OUTPUT);
  pinMode(PWR_DVL, OUTPUT);
  pinMode(PWR_LNA, OUTPUT);
  pinMode(PWR_IMU, OUTPUT);
  pinMode(PWR_HUB, OUTPUT);
  pinMode(PWR_FOG_IMU, OUTPUT);
  pinMode(PWR_LAN, OUTPUT);
  pinMode(PWR_SPARE, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);
  Set_dir_pin(DIR_PIN);  
  MBCore.Begin(0,0,0,1,DEV_ADDR);
  control.rawData = 0;
  
  // Fixed control
  digitalWrite(LED_PIN,HIGH);
  digitalWrite(DIR_PIN,LOW);  
  digitalWrite(PWR_LAN,HIGH);
}

void loop() {
    MBCore.Run(); // Must call at least every 1ms 
    MBCore.Read_holding_register(&(control.rawData), 0, 1);
    if (control.pwr_pc0) digitalWrite(PWR_PC0,HIGH);
    else digitalWrite(PWR_PC0,LOW);
    if (control.pwr_pc1) digitalWrite(PWR_PC1,HIGH);
    else digitalWrite(PWR_PC1,LOW);
    if (control.pwr_sonar) digitalWrite(PWR_SONAR,HIGH);
    else digitalWrite(PWR_SONAR,LOW);
    if (control.pwr_altimeter) digitalWrite(PWR_ALTIMETER,HIGH);
    else digitalWrite(PWR_ALTIMETER,LOW);
    if (control.pwr_dsp) digitalWrite(PWR_DSP,HIGH);
    else digitalWrite(PWR_DSP,LOW);
    if (control.pwr_dvl) digitalWrite(PWR_DVL,HIGH);
    else digitalWrite(PWR_DVL,LOW);
    if (control.pwr_lna) digitalWrite(PWR_LNA,HIGH);
    else digitalWrite(PWR_LNA,LOW);
    if (control.pwr_imu) digitalWrite(PWR_IMU,HIGH);
    else digitalWrite(PWR_IMU,LOW);
    if (control.pwr_hub) digitalWrite(PWR_HUB,HIGH);
    else digitalWrite(PWR_HUB,LOW);
    if (control.pwr_fog_imu) digitalWrite(PWR_FOG_IMU,HIGH);
    else digitalWrite(PWR_FOG_IMU,LOW);    
    //if (control.pwr_lan) digitalWrite(PWR_LAN,HIGH);
    //else digitalWrite(PWR_LAN,LOW);
    if (control.pwr_spare) digitalWrite(PWR_SPARE,HIGH);
    else digitalWrite(PWR_SPARE,LOW);
  }



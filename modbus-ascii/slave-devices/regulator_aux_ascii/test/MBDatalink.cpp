#include "MBDatalink.h"
#define SHOWDEBUG if(show_debug_info)printf

const char* portname = "/dev/ttyUSB0"; 		// Default port.
int read_timeout = 1; 						// Wait time for data to be available in receive buffer (tens of second).
int fd;										// File descriptor number of port.
int port_openned = 0;						// Port status.
int lrc_correct_count = 0;					// Valid checksum count.
int lrc_incorrect_count = 0;				// Invalid checksum count.
unsigned int total_request_count = 0;		// Total request count.
unsigned int request_success_count = 0;		// Request success count.
struct timeval timeout;						// Timeout for select() system call.
struct timeval select_timeout = {1, 0};	    // Defaut timeout for select() system call.
int show_debug_info = 1;					// Show debug infomation
//int transmission_timeout = 100;			// Transmission timeout (microseconds)

int Set_interface_attribs (int fd, int speed, int parity)
{
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
            SHOWDEBUG("error %d from tcgetattr\n", errno);
            return -1;
    }

    cfsetospeed (&tty, speed);
    cfsetispeed (&tty, speed);
    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS7;     // 7-bit chars
    //tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    tty.c_iflag &= ~IGNBRK;         // disable break processing
    tty.c_lflag = 0;                // no signaling chars, no echo,	
                                    // no canonical processing
    tty.c_oflag = 0;                // no remapping, no delays
    tty.c_cc[VMIN]  = 0;            // read doesn't block
    tty.c_cc[VTIME] = read_timeout;	// Next charater wait timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY | INLCR | IGNCR | ICRNL); // shut off xon/xoff ctrl and cr to lf auto repalce.

    tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                    // enable reading
    //tty.c_cflag |= PARENB;  		// Enable even parity
    //tty.c_cflag &= ~CSTOPB; 		// one stopbit
    tty.c_cflag |= CSTOPB; 			// 2 stopbit
    tty.c_cflag &= ~CRTSCTS;		// No RTS CTS control signal
    tty.c_cflag &= ~HUPCL;			// Prevent restarting in arduino.
    if (tcsetattr (fd, TCSANOW, &tty) != 0)
    {
            SHOWDEBUG("error %d from tcsetattr\n", errno);
            return -1;
    }
    return 0;
}

void Set_blocking (int fd, int should_block)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                SHOWDEBUG ("error %d from tggetattr\n", errno);
                return;
        }

        tty.c_cc[VMIN]  = should_block ? 1 : 0;
        tty.c_cc[VTIME] = read_timeout;            // 0.5 seconds read timeout

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
                SHOWDEBUG ("error %d setting term attributes\n", errno);
}
	
uint16_t Modbus_comm(frame_buffer* tx_frame_buffer, frame_buffer* rx_frame_buffer){
	Open_port();
	if (fd<0){
		SHOWDEBUG("Port not opened: %s \n", portname);
		return 0;
	}
	int i;
	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(fd, &fds);
	tx_frame_buffer->raw_data[tx_frame_buffer->len] = '\0';
	int wr = write (fd, tx_frame_buffer->raw_data, tx_frame_buffer->len); 
	SHOWDEBUG("Transmit (%d bytes) : ",wr);
	Print_raw_frame(tx_frame_buffer);          // send frame

	struct timeval before , after;
    gettimeofday(&before , NULL);

	if (wr!=tx_frame_buffer->len) // Write attemp 2 times.
	{
		wr = write (fd, tx_frame_buffer->raw_data, tx_frame_buffer->len);
		if (wr!=tx_frame_buffer->len) 
		{
			SHOWDEBUG("Send frame error\n");
			close(fd);
			return 0;
		}
		
	}
	timeout = select_timeout;  // Reset select timeout.
	int ret =select(fd+1, &fds, NULL, NULL, &timeout);
	 /* ret == 0 means timeout, ret == 1 means descriptor is ready for reading,
	 ret == -1 means error (check errno) */
	if (ret ==1){		
		//int rr = read (fd, rx_frame_buffer->raw_data, SERIAL_BUFFER_SIZE);
		int rr = Serial_read (fd, rx_frame_buffer->raw_data, SERIAL_BUFFER_SIZE); // N

		/* Roundtrip time measurement */
		gettimeofday(&after , NULL);
		double dif = time_diff(before , after);
		/*-----------------------------*/

		/* Construct received data into frame */
		uint16_t count=0;
		int frame_ended = 0;
		SHOWDEBUG("Received response (%d bytes) ", rr);
		for (i=0;i<rr;i++)
		{
			SHOWDEBUG("%c",rx_frame_buffer->raw_data[i]);
			if(rx_frame_buffer->raw_data[i]=='\n')
			{
				frame_ended = 1;
				count++;
				break;
			}else{
				count++;
			}
			
		}
		SHOWDEBUG("Roundtrip time %.0lf usec\n", dif);
		if (frame_ended!=1) count = 0;
		rx_frame_buffer->len = count; 
		return count;
		/*--------------------------------------*/
	}
	else if(ret == 0) {
		SHOWDEBUG("timeout error fd=%d\n",fd);
	}
	else {
		SHOWDEBUG("some other error\n");
	}
	//close(fd);
	return 0;
}

unsigned int Modbus_request(uint8_t slave_address, pdu_t* req_pdu, pdu_t* resp_pdu, uint16_t pdu_len)
{
	SHOWDEBUG("============ Transaction started =============================================================\n");
	total_request_count++;
	frame_buffer tx_frame_buffer;
	frame_buffer rx_frame_buffer;
	bin_frame_t tx_bin_frame;
	bin_frame_t rx_bin_frame;
	tx_bin_frame.address = slave_address;
	tx_bin_frame.pdu = *req_pdu;
	Lrc_calc(&tx_bin_frame);
	tx_bin_frame.pdu_len = pdu_len;
	//Print_bin_frame(&tx_bin_frame);
	Bin_to_hexstring_frame(&tx_bin_frame, &tx_frame_buffer);
	// MB raw frame ready to transmit
	uint16_t raw_frame_size = Modbus_comm(&tx_frame_buffer, &rx_frame_buffer);
	if (raw_frame_size<7)
	{ 
		SHOWDEBUG("Read fail\n");
		return 0;
	}
	Hexstring_to_bin_frame(&rx_bin_frame, &rx_frame_buffer, raw_frame_size);
	uint8_t lrc = rx_bin_frame.lrc;
	Lrc_calc(&rx_bin_frame);
	if (rx_bin_frame.lrc == lrc)
	{
		SHOWDEBUG("  LRC Correct [%.02x]\n", rx_bin_frame.lrc);
		lrc_correct_count++;
	}
	else{
		SHOWDEBUG("  LRC Incorrect [%.02x] expected [%.02x] " , rx_bin_frame.lrc, lrc);
		lrc_incorrect_count++;
		return 0;
	}
	*resp_pdu = rx_bin_frame.pdu;
	request_success_count++;
	return rx_bin_frame.pdu_len; 

}

void Hexstring_to_bin_frame(bin_frame_t* bin_frame, frame_buffer* hexstring_frame, uint16_t raw_frame_size){
	bin_frame->address = Hexstring_to_bin(hexstring_frame->raw_data[1], hexstring_frame->raw_data[2]);
	bin_frame->pdu.function = Hexstring_to_bin(hexstring_frame->raw_data[3],hexstring_frame->raw_data[4]);
	uint8_t i;
	for (i=5;i<hexstring_frame->len-4;i+=2)
	{
		bin_frame->pdu.data[(i-5)/2] = Hexstring_to_bin(hexstring_frame->raw_data[i],hexstring_frame->raw_data[i+1]);
	}
	bin_frame->lrc = Hexstring_to_bin(hexstring_frame->raw_data[hexstring_frame->len-4],hexstring_frame->raw_data[hexstring_frame->len-3]);
	bin_frame->pdu_len = (hexstring_frame->len-7)/2;
}


uint8_t Hexstring_to_bin(uint8_t high, uint8_t low)
{
	uint8_t tmp_h, tmp_l;
	if (high >='0' && high <= '9')
	{
		tmp_h = high - '0';
	}else 	if (high >= 'A' && high <= 'F')
			{
				tmp_h = high - 'A' + 10;
			}
	else tmp_h = 0;
	if (low >= '0' && low <= '9')
	{
		tmp_l = low - '0';
	}else 	if (low >= 'A' && low <= 'F')
			{
				tmp_l = low - 'A' + 10;
			}
	else tmp_l = 0;
	return (tmp_h << 4) | tmp_l;
}

void Bin_to_hexstring_frame(bin_frame_t* bin_frame, frame_buffer* hexstring_frame)
{
	hexstring_frame->raw_data[0] = ':';
	Bin_to_hexstring(&(bin_frame->address), &(hexstring_frame->raw_data[1]), &(hexstring_frame->raw_data[2]) );
	Bin_to_hexstring(&(bin_frame->pdu.function), &(hexstring_frame->raw_data[3]), &(hexstring_frame->raw_data[4]) );
	uint8_t i;
	for (i=0;i<bin_frame->pdu_len-1;i++)
	{
		Bin_to_hexstring( &(bin_frame->pdu.data[i]), &(hexstring_frame->raw_data[2*i+5]), &(hexstring_frame->raw_data[2*i+6]) );
	}
	Lrc_calc(bin_frame);
	Bin_to_hexstring( &(bin_frame->lrc), &(hexstring_frame->raw_data[2*i+5]), &(hexstring_frame->raw_data[2*i+6]) );
	hexstring_frame->raw_data[2*i+7] = '\r';
	hexstring_frame->raw_data[2*i+8] = '\n';
	hexstring_frame->pos = 0;
	hexstring_frame->len = (bin_frame->pdu_len)*2+7;
	
}

void Bin_to_hexstring(uint8_t* bin, uint8_t* high, uint8_t* low)
{
	uint8_t tmp = (*bin) >> 4;
	*high = (tmp >= 0 && tmp <= 9)?(tmp+'0'):(tmp+'A'-10);
	tmp = (*bin) & 15;
	*low = (tmp >= 0 && tmp <= 9)?(tmp+'0'):(tmp+'A'-10);
}

void Lrc_calc(bin_frame_t* frame)
{
	uint8_t sum=0;
	uint8_t lrc_valid=0;
	sum += frame->address;
	sum += frame->pdu.function;
	uint8_t i;
	for (i=0;i<frame->pdu_len-1;i++)	
	{
		sum += frame->pdu.data[i];			
	}
	frame->lrc = (~sum) + 1; 
}
void Print_raw_frame(frame_buffer* frame)
{
	int i;
	for(i=0;i<frame->len;i++)
	{
		SHOWDEBUG("%c",(char)frame->raw_data[i]);
	}
}
void Print_bin_frame(bin_frame_t* frame)
{
	SHOWDEBUG("\\x%.02X",(int)frame->address);
	SHOWDEBUG("\\x%.02X",(int)frame->pdu.function);
	int i;
	for (i=0;i<frame->pdu_len-1;i++)
	{
		SHOWDEBUG("\\x%.02X",frame->pdu.data[i]);
	}
	SHOWDEBUG("\\x%.02X",(int)frame->lrc);
	SHOWDEBUG("   (len=%d)\n",frame->pdu_len);
}
void Set_portname(const char* _portname)
{
	portname = _portname;
}
void Set_select_timeout(int usec)
{
	select_timeout.tv_sec = usec/1000000;
	select_timeout.tv_usec = usec%1000000;
}
unsigned int Open_port()
{
	if (port_openned) return 1;

	fd = open (portname, O_RDWR | O_NOCTTY | O_SYNC);
	if (fd < 0)
	{
	        SHOWDEBUG ("error %d opening %s: %s\n", errno, portname, strerror (errno));
	        close(fd);
	        return 0;
	}
	Set_interface_attribs (fd, B1000000, 0);    // set speed to 1000000 bps, 7n2 (even parity)
	Set_blocking (fd, 0);               		// set no blocking
	port_openned = 1;
	SHOWDEBUG("Openned port (fd = %d)\n", fd);	
	//usleep(500000);
	//usleep(500000);
	//usleep(500000);
	return 1;
}
void Close_port()
{
	port_openned = 0;
	close(fd);
}
double time_diff(struct timeval x , struct timeval y)
{
    double x_ms , y_ms , diff;
     
    x_ms = (double)x.tv_sec*1000000 + (double)x.tv_usec;
    y_ms = (double)y.tv_sec*1000000 + (double)y.tv_usec;
     
    diff = (double)y_ms - (double)x_ms;
     
    return diff;
}
int Serial_read(int read_fd, uint8_t* buf, int size_to_read)
{
	int count = 0;;
	while (read (read_fd, buf+count, 1)>0 && buf[count] != '\n' && count < size_to_read)
	{
		count++;
	}
	if (buf[count] == '\n') count++;
	return count;
}

int Serial_print(char* data, int size)
{
	Open_port();
	if (fd<0){
		SHOWDEBUG("Port not opened: %s \n", portname);
		return 0;
	}
	int wr = write (fd, data, size);
	if (wr)
	{
		SHOWDEBUG("Transmitted %d bytes. ", wr);
		int i;
		for (i=0;i<wr;i++)
		{
			SHOWDEBUG("%c",data[i]);
		}
		SHOWDEBUG("\n");
	}else{
	SHOWDEBUG("Send frame error.\n");
			return 0;
	}


}

#include <MBProtocol.h>
#include <MBDatalink.h>
#include <MBDatatypes.h>
#include <MBSerialMega328.h>
#include <MBDevice.h>

/* Regulator Aux */


#define DEV_ADDR   0x11
#define PWR0       A0
/*
#define M0         A1
#define M1         A2
#define M2         A3
#define M3         A4
#define M4         3
#define M5         4
#define M6         5
#define M7         6
*/
#define M0         6
#define M1         5
#define M2         4
#define M3         3
#define M4         A4
#define M5         A3
#define M6         A2
#define M7         A1

#define M8         7
#define M9         2
#define GPIO_EN    8
#define T_LOGIC_EN 9
#define LED_PIN    10
#define D11        11
#define D12        12
#define DIR_PIN    A5   

#define PWR_M0          M0
#define PWR_M1          M1
#define PWR_M2          M2
#define PWR_M3          M3
#define PWR_M4          M4
#define PWR_M5          M5
#define PWR_M6          M6
#define PWR_M7          M7
#define PWR_PNEUMATIC   M8
#define PWR_SPARE       M9
#define PWR_MAIN        PWR0
#define PWR_GPIO        GPIO_EN
#define PWR_T_LOGIC     T_LOGIC_EN
#define PWR_LAMP1           D11
#define PWR_LAMP2           D12

uint16_t lamp1_val, lamp2_val;

struct reg_aux_control_t
{
   union
   {
       struct
       {
          uint16_t pwr_m0           :1;
          uint16_t pwr_m1           :1;
          uint16_t pwr_m2           :1;
          uint16_t pwr_m3           :1;
          uint16_t pwr_m4           :1;
          uint16_t pwr_m5           :1;
          uint16_t pwr_m6           :1;
          uint16_t pwr_m7           :1;
          uint16_t pwr_pneumatic    :1;
          uint16_t pwr_spare        :1;
          uint16_t pwr_main         :1;
          uint16_t pwr_gpio         :1;
          uint16_t pwr_t_logic      :1;
          uint16_t pwr_lamp1        :1;
          uint16_t pwr_lamp2        :1;
          uint16_t unused           :1;
       };

       uint16_t rawData;
   };
};
reg_aux_control_t control;

void setup() {
  pinMode(M0, OUTPUT);
  pinMode(M1, OUTPUT);
  pinMode(M2, OUTPUT);
  pinMode(M3, OUTPUT);
  pinMode(M4, OUTPUT);
  pinMode(M5, OUTPUT);
  pinMode(M6, OUTPUT);
  pinMode(M7, OUTPUT);
  pinMode(M8, OUTPUT);
  pinMode(M9, OUTPUT);
  //pinMode(PWR0, OUTPUT);
  pinMode(GPIO_EN, OUTPUT);
  pinMode(T_LOGIC_EN, OUTPUT);    
  pinMode(D11, OUTPUT);
  pinMode(D12, OUTPUT);
  Set_dir_pin(DIR_PIN);
  pinMode(LED_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);
  
  MBCore.Begin(0,0,0,3,DEV_ADDR);
  control.rawData = 0;
  digitalWrite(LED_PIN,LOW);
  digitalWrite(DIR_PIN,LOW);
  
}

void loop() {
    MBCore.Run(); // Must call at least every 1ms
    //MBCore.Read_holding_register(&lamp1_val, 1, 1); 
    //MBCore.Read_holding_register(&lamp2_val, 2, 1);
    //analogWrite(PWR_LAMP1, lamp1_val);
    //analogWrite(PWR_LAMP2, lamp2_val);
    //analogWrite(LED_PIN, 255-lamp1_val);
    MBCore.Read_holding_register(&(control.rawData), 0, 1);
    digitalWrite(PWR_M0, (control.pwr_m0)?HIGH:LOW);
    digitalWrite(PWR_M1, (control.pwr_m1)?HIGH:LOW);
    digitalWrite(PWR_M2, (control.pwr_m2)?HIGH:LOW);
    digitalWrite(PWR_M3, (control.pwr_m3)?HIGH:LOW);
    digitalWrite(PWR_M4, (control.pwr_m4)?HIGH:LOW);
    digitalWrite(PWR_M5, (control.pwr_m5)?HIGH:LOW);
    digitalWrite(PWR_M6, (control.pwr_m6)?HIGH:LOW);
    digitalWrite(PWR_M7, (control.pwr_m7)?HIGH:LOW);
    digitalWrite(PWR_PNEUMATIC, (control.pwr_pneumatic)?HIGH:LOW);
    digitalWrite(PWR_SPARE, (control.pwr_spare)?HIGH:LOW);
    //digitalWrite(PWR_MAIN, (control.pwr_main)?HIGH:LOW);
    digitalWrite(PWR_GPIO, (control.pwr_gpio)?HIGH:LOW);
    digitalWrite(PWR_T_LOGIC, (control.pwr_t_logic)?HIGH:LOW);
    digitalWrite(PWR_LAMP1, (control.pwr_lamp1)?HIGH:LOW);
    digitalWrite(PWR_LAMP2, (control.pwr_lamp2)?HIGH:LOW);
  }



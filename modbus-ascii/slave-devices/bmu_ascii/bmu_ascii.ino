#include <MBDatatypes.h>
#include <MBProtocol.h>
#include <MBDevice.h>
#include <MBDatalink.h>
#include <MBSerialMega328.h>
#include <stdint.h>
#include <avr/sleep.h>
#include <Wire.h>
#include <Endian.h>

#define DEV_ADDR     0x20
#define LTC2943      100       // Default address of LTC2943
#define LED_PIN      A0
#define DIR_PIN      2
#define WAKE_PIN     3
#define INTERVAL     400UL

/* Default parameter */ 

#define ACTIVE_CONTROL  0b11100000  // Automatic mode, prescaler 256, ALCC disabled, power on
#define SLEEP_CONTROL   0b00100000  // Sleep mode, prescaler 256, ALCC disabled, power on

#define CHARGE_THRESHOLD_HIGH        0xCFFF  // @ 100%
#define CHARGE_THRESHOLD_LOW         0x29FB  // @ 0%
#define CHARGE_PRESET                0xCFFF  // @ 100%
#define VOLTAGE_THRESHOLD_HIGH       0xB63C  // @ 16.80 V
#define VOLTAGE_THRESHOLD_LOW        0xA2B6  // @ 15.00 V
#define CURRENT_THRESHOLD_HIGH       0xD535  // @ 80 A 
#define CURRENT_THRESHOLD_LOW        0x2A8C  // @ -10A (charge)
#define TEMPERATURE_THRESHOLD_HIGH   0xA7    // @ 60Deg-Celcius
#define TEMPERATURE_THRESHOLD_LOW    0x00    // @ 60Deg-Celcius

#define readStatus(VAR)      readLTC2943(0x00, (unsigned char* )&VAR, 1)
#define readCharge(VAR)      readLTC2943(0x02, (unsigned char* )&VAR, 2);swapEndian(&VAR)
#define readVoltage(VAR)     readLTC2943(0x08, (unsigned char* )&VAR, 2);swapEndian(&VAR)
#define readCurrent(VAR)     readLTC2943(0x0E, (unsigned char* )&VAR, 2);swapEndian(&VAR)
#define readTemperature(VAR) readLTC2943(0x14, (unsigned char* )&VAR, 2);swapEndian(&VAR)

#define readControl(VAR)     readLTC2943(0x01, (unsigned char* )&VAR, 1)
#define readLimitQH(VAR)     readLTC2943(0x04, (unsigned char* )&VAR, 2);swapEndian(&VAR)
#define readLimitQL(VAR)     readLTC2943(0x06, (unsigned char* )&VAR, 2);swapEndian(&VAR)
#define readLimitVH(VAR)     readLTC2943(0x0A, (unsigned char* )&VAR, 2);swapEndian(&VAR)
#define readLimitVL(VAR)     readLTC2943(0x0C, (unsigned char* )&VAR, 2);swapEndian(&VAR)
#define readLimitCH(VAR)     readLTC2943(0x10, (unsigned char* )&VAR, 2);swapEndian(&VAR)
#define readLimitCL(VAR)     readLTC2943(0x12, (unsigned char* )&VAR, 2);swapEndian(&VAR)
#define readLimitTH(VAR)     readLTC2943(0x16, (unsigned char* )&VAR, 1)
#define readLimitTL(VAR)     readLTC2943(0x17, (unsigned char* )&VAR, 1)

#define writeControl(VAR)    writeLTC2943(0x01, (unsigned char* )&VAR, 1)
#define writeCharge(VAR)     swapEndian(&VAR); writeLTC2943(0x02, (unsigned char* )&VAR, 2)
#define writeLimitQH(VAR)    swapEndian(&VAR); writeLTC2943(0x04, (unsigned char* )&VAR, 2)
#define writeLimitQL(VAR)    swapEndian(&VAR); writeLTC2943(0x06, (unsigned char* )&VAR, 2)
#define writeLimitVH(VAR)    swapEndian(&VAR); writeLTC2943(0x0A, (unsigned char* )&VAR, 2)
#define writeLimitVL(VAR)    swapEndian(&VAR); writeLTC2943(0x0C, (unsigned char* )&VAR, 2)
#define writeLimitCH(VAR)    swapEndian(&VAR); writeLTC2943(0x10, (unsigned char* )&VAR, 2)
#define writeLimitCL(VAR)    swapEndian(&VAR); writeLTC2943(0x12, (unsigned char* )&VAR, 2)
#define writeLimitTH(VAR)    writeLTC2943(0x16, (unsigned char* )&VAR, 1)
#define writeLimitTL(VAR)    writeLTC2943(0x17, (unsigned char* )&VAR, 1)

uint8_t sleep_demand = 1;
unsigned long wake_time_stamp = 0;
unsigned long current_time = 0;
uint8_t tmp_byte;
uint16_t tmp_word;
uint8_t tmp_byte2;

void readLTC2943(byte addr, unsigned char* buf, int len) {
  uint8_t i = 0;
  Wire.beginTransmission(LTC2943);  // Invoke Start transmission on LTC2943
  Wire.write(addr);                  // Send in register's address to read
  Wire.endTransmission(false);
  Wire.requestFrom(LTC2943, len, true);
  while (Wire.available()) {
    buf[i++] = Wire.read();
  }
  Wire.endTransmission(true);
}

void writeLTC2943(byte addr, const unsigned char* buf, int len) {           
  Wire.beginTransmission(LTC2943);  // Invoke Start transmission on LTC2943
  Wire.write(addr);                  // Send in register's address to write
  Wire.write(buf, len);
  Wire.endTransmission(true);
}

void Pin3_isr()
{
     return;
}

void sleepNow()         // here we put the arduino to sleep
{
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);   // Minimum power consumption mode 
    sleep_enable();          // enables the sleep bit in the mcucr register
    tmp_byte = SLEEP_CONTROL;
    writeControl(tmp_byte);
    ISRSerial.clearBuffer(); //Abort transmission before sleep 
    attachInterrupt(1, Pin3_isr, LOW); // use interrupt 1 (pin 3)
    digitalWrite(LED_PIN, LOW);
    digitalWrite(DIR_PIN, LOW);
    pinMode(LED_PIN, INPUT);  
    pinMode(DIR_PIN, INPUT); 
    sleep_mode();            // here the device is actually put to sleep!!
                             // THE PROGRAM CONTINUES FROM HERE AFTER WAKING UP

    sleep_disable();         // first thing after waking from sleep: Disable sleep.
    detachInterrupt(1);      // disables interrupt 1 on pin 3 so the
                             // wakeUpNow code will not be executed
                             // during normal running time.
    tmp_byte = ACTIVE_CONTROL;
    writeControl(tmp_byte);
    pinMode(DIR_PIN, OUTPUT);
    pinMode(LED_PIN, OUTPUT);
    digitalWrite(LED_PIN, HIGH);
    wake_time_stamp = millis();
 
}

void updateRegister()
{
       /* Update status and data in memory from LTC2943 */
    MBCore.Read_coil(&tmp_byte2, 0, 8);
    readStatus(tmp_byte);
    tmp_byte2 |= tmp_byte;
    MBCore.Set_coil(&tmp_byte2, 0, 8);
    readVoltage(tmp_word);
    MBCore.Set_input_register(&tmp_word, 0, 1);
    readCurrent(tmp_word);
    MBCore.Set_input_register(&tmp_word, 1, 1);
    readTemperature(tmp_word);
    MBCore.Set_input_register(&tmp_word, 2, 1);
    readCharge(tmp_word);
    MBCore.Set_input_register(&tmp_word, 3, 1);
}

void presetLTC2943()
{
 
  
  tmp_byte = ACTIVE_CONTROL;
  writeControl(tmp_byte);
  MBCore.Set_discrete_input(&tmp_byte, 0, 8);

  tmp_word = CHARGE_PRESET;
  writeCharge(tmp_word);
  MBCore.Set_input_register(&tmp_word, 0, 1);
  
  tmp_word = CHARGE_THRESHOLD_HIGH;
  writeLimitQH(tmp_word);
  MBCore.Set_input_register(&tmp_word, 1, 1);
  
  tmp_word = CHARGE_THRESHOLD_LOW;
  writeLimitQL(tmp_word);
  MBCore.Set_input_register(&tmp_word, 2, 1);
  
  tmp_word = VOLTAGE_THRESHOLD_HIGH;
  writeLimitVH(tmp_word);
  MBCore.Set_input_register(&tmp_word, 3, 1);
  
  tmp_word = VOLTAGE_THRESHOLD_LOW;
  writeLimitVL(tmp_word);
  MBCore.Set_input_register(&tmp_word, 4, 1);
  
  tmp_word = CURRENT_THRESHOLD_HIGH;
  writeLimitCH(tmp_word);
  MBCore.Set_input_register(&tmp_word, 5, 1);
  
  tmp_word = CURRENT_THRESHOLD_LOW;
  writeLimitCL(tmp_word);
  MBCore.Set_input_register(&tmp_word, 6, 1);
  
  tmp_byte = TEMPERATURE_THRESHOLD_HIGH;
  writeLimitTH(tmp_byte);
  tmp_word = tmp_byte;
  MBCore.Set_input_register(&tmp_word, 7, 1);
  
  tmp_byte = TEMPERATURE_THRESHOLD_LOW;
  writeLimitTL(tmp_byte);
  tmp_word = tmp_byte;
  MBCore.Set_input_register(&tmp_word, 8, 1);

 
}
void trigger(bin_frame_t* rx, bin_frame_t* tx)
{
  updateRegister();
}

void setup() {
  MBCore.Begin(8,8,13,0,DEV_ADDR);
  Set_dir_pin(DIR_PIN);
  Wire.begin();
  pinMode(LED_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);
  pinMode(WAKE_PIN, INPUT);  
  presetLTC2943();  
  MB_protocol.Attach_trigger(trigger);
}

void loop() {   
   current_time = millis();
   if (current_time - wake_time_stamp > INTERVAL)
   {
         sleepNow(); 
   }
   if (MBCore.Run()) wake_time_stamp = millis();;   
}


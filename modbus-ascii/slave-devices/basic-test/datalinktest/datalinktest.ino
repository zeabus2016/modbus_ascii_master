#include <MBProtocol.h>
#include <MBDatalink.h>
#include <MBDatatypes.h>
#include <MBSerialMega328.h>
#include <MBDevice.h>

void setup() {  
    DDRB |= (1<<PB2);
    PORTB &= ~(1<<PB2);
    DDRD |= (1<<PD2);
    MBCore.Begin(20,20,20,20,0x10);
    uint8_t test[2] = {0xFF, 0xFF};
    MBCore.Set_coil(test, 0, 16);
    
}

void loop() {
  MBCore.Run(); // Must call at least every 1ms 
  uint8_t test[2] = {0xAA, 0xAA};
  MBCore.Read_coil(test,0,16);
  MBCore.Set_discrete_input(test, 0, 16);
  uint16_t registers[20];
  MBCore.Read_holding_register(registers, 0, 20);
  MBCore.Set_input_register(registers, 0, 20);
}

/*
 * zeabus_modbus_master.cpp
 *
 *  Created on: Jun 15, 2015
 *      Author: mahisorn
 */
#include <ros/ros.h>
#include <sensor_msgs/FluidPressure.h>
#include <modbus_ascii_ros/SendCommand.h>
#include <modbus_ascii_ros/Switch.h>

#include <mblib/MBDevice.h>

#include <boost/thread.hpp>

#include <termios.h>

#define REG_MAIN_ADDR 0x10
#define REG_AUX_ADDR  0x11
#define GPIO_ADDR     0x12
#define ON 1
#define OFF 0

/*
MBSlave mb_slave_regulator_aux(REG_AUX_ADDR);
MBSlave mb_slave_regulator_main(REG_MAIN_ADDR);
MBSlave mb_slave_gpio(GPIO_ADDR);
*/

boost::mutex g_mutex;
ros::Publisher g_pub_baro;
ros::Publisher g_pub_switch;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "hg_modbus_master");
  ros::NodeHandle nh("~");

  std::string device,device2;
  int baudrate;
  std::string frame_id;

  nh.param<std::string>("device", device, "/dev/ttyACM0");
  nh.param<std::string>("device2", device2, "/dev/ttyACM1");
  //nh.param<std::string>("frame_id", frame_id, "baro_link");
  //Set_portname(device.c_str());
  //Open_port(B115200, 0, 8, 1);
#if 1
  //Open_port();
  int fd = 0;
  int fd2 = 0;
  Open_port(&fd, device.c_str());

  //Open_port(&fd2, device2.c_str());

  if(fd <= 0)
  {
    ROS_FATAL_STREAM("Cannot open " << device);
    exit(1);
  }

  //if (fd2 <= 0)
  //{
    //ROS_FATAL_STREAM("Cannot open " << device2);
    //exit(1);
  //}

  MBSlave* motor_board = new MBSlave(18, device.c_str(), "", B115200, 8, 0, 1);
  motor_board->Init(fd);

  //MBSlave* //motor_board2 = new MBSlave(18, device2.c_str(), "", B115200, 8, 0, 1);
  //motor_board2->Init(fd2);

  int32_t tmp;

  motor_board->Write_single_coil(8, 0);                  //Select DC motor
  //motor_board2->Write_single_coil(8, 0);                  //Select DC motor

  tmp = 255;
  motor_board->Write_multiple_registers(50, 2, (uint16_t*)&tmp);           //Set M0 PWM limit
  motor_board->Write_multiple_registers(50+64, 2, (uint16_t*)&tmp);        //Set M1 PWM limit
  motor_board->Write_single_coil(0, 1);                  //Enable control M0
  motor_board->Write_single_coil(0+4, 1);                //Enable control M1
  motor_board->Write_single_coil(2, 1);                  //Velocity mask enable M0
  motor_board->Write_single_coil(2+4, 1);                //Velocity mask enable M1

  //motor_board2->Write_multiple_registers(50, 2, (uint16_t*)&tmp);           //Set M0 PWM limit
  //motor_board2->Write_multiple_registers(50+64, 2, (uint16_t*)&tmp);        //Set M1 PWM limit
  //motor_board2->Write_single_coil(0, 1);                  //Enable control M0
  //motor_board2->Write_single_coil(0+4, 1);                //Enable control M1
  //motor_board2->Write_single_coil(2, 1);                  //Velocity mask enable M0
  //motor_board2->Write_single_coil(2+4, 1);                //Velocity mask enable M1

  tmp = 20;
  motor_board->Write_multiple_registers(18, 2, (uint16_t*)&tmp);           //M0 kp
  //motor_board2->Write_multiple_registers(18, 2, (uint16_t*)&tmp);           //M0 kp
  tmp = 5;
  motor_board->Write_multiple_registers(20, 2, (uint16_t*)&tmp);             //M0 ki
  //motor_board2->Write_multiple_registers(20, 2, (uint16_t*)&tmp);             //M0 ki
  tmp = 1;
  motor_board->Write_multiple_registers(22, 2, (uint16_t*)&tmp);             //M0 kd
  //motor_board2->Write_multiple_registers(22, 2, (uint16_t*)&tmp);             //M0 kd
  tmp = 800;
  motor_board->Write_multiple_registers(24, 2, (uint16_t*)&tmp);         //M0 il
  //motor_board2->Write_multiple_registers(24, 2, (uint16_t*)&tmp);         //M0 il
  tmp = 120;
  motor_board->Write_multiple_registers(26, 2, (uint16_t*)&tmp);         //M0 output limit
  //motor_board2->Write_multiple_registers(26, 2, (uint16_t*)&tmp);         //M0 output limit
  tmp = 3;
  motor_board->Write_multiple_registers(28, 2, (uint16_t*)&tmp);             //M0 output scaling
  //motor_board2->Write_multiple_registers(28, 2, (uint16_t*)&tmp);             //M0 output scaling

  tmp = 20;
  motor_board->Write_multiple_registers(18+64, 2, (uint16_t*)&tmp);           //M0 kp
  //motor_board2->Write_multiple_registers(18+64, 2, (uint16_t*)&tmp);           //M0 kp
  tmp = 5;
  motor_board->Write_multiple_registers(20+64, 2, (uint16_t*)&tmp);             //M0 ki
  //motor_board2->Write_multiple_registers(20+64, 2, (uint16_t*)&tmp);             //M0 ki
  tmp = 1;
  motor_board->Write_multiple_registers(22+64, 2, (uint16_t*)&tmp);             //M0 kd
  //motor_board2->Write_multiple_registers(22+64, 2, (uint16_t*)&tmp);             //M0 kd
  tmp = 800;
  motor_board->Write_multiple_registers(24+64, 2, (uint16_t*)&tmp);         //M0 il
  //motor_board2->Write_multiple_registers(24+64, 2, (uint16_t*)&tmp);         //M0 il
  tmp = 120;
  motor_board->Write_multiple_registers(26+64, 2, (uint16_t*)&tmp);         //M0 output limit
  //motor_board2->Write_multiple_registers(26+64, 2, (uint16_t*)&tmp);         //M0 output limit
  tmp = 3;
  motor_board->Write_multiple_registers(28+64, 2, (uint16_t*)&tmp);             //M0 output scaling
  //motor_board2->Write_multiple_registers(28+64, 2, (uint16_t*)&tmp);             //M0 output scaling


  motor_board->Write_single_coil(13, 1);                  //Enable driver
  //motor_board2->Write_single_coil(13, 1);                  //Enable driver

  int32_t speed = -10;

  motor_board->Write_multiple_registers(16, 2, ((uint16_t*)(&speed)));
  motor_board->Write_multiple_registers(16+64, 2, ((uint16_t*)(&speed)));
  //motor_board2->Write_multiple_registers(16, 2, ((uint16_t*)(&speed)));
  //motor_board2->Write_multiple_registers(16+64, 2, ((uint16_t*)(&speed)));


  /*
  ros::ServiceServer service_set_aux = nh.advertiseService("set_regulator_aux", setRegulatorAux);
  ros::ServiceServer service_set_main = nh.advertiseService("set_regulator_main", setRegulatorMain);
  ros::ServiceServer service_set_gpio = nh.advertiseService("set_gpio", setGPIO);

  g_pub_baro = nh.advertise<sensor_msgs::FluidPressure>("baro/data", 10);
  g_pub_switch = nh.advertise<modbus_ascii_ros::Switch>("switch/data", 10);
  */

  ros::Rate rate(10);
  int32_t posM0, posM1;
  int32_t speedM0, speedM1;

  while(ros::ok())
  {
    //read baro


    //read switch


    //publish baro

    //publish switch
/*
    motor_board->Read_input_registers(0, 2, (uint16_t*)(&posM0));
    motor_board->Read_input_registers(0+32, 2, (uint16_t*)(&posM1));
    motor_board->Read_input_registers(2, 2, (uint16_t*)(&speedM0));
    motor_board->Read_input_registers(2+32, 2, (uint16_t*)(&speedM1));

    ROS_INFO("M0 p:%d v:%d M1 p:%d v:%d", posM0, speedM0, posM1, speedM1);



    motor_board->Write_multiple_registers(16, 2, ((uint16_t*)(&speed)));
    motor_board->Write_multiple_registers(16+64, 2, ((uint16_t*)(&speed)));
*/

    rate.sleep();
    ros::spinOnce();
  }


#endif



  return 0;
}


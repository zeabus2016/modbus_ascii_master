/*
 * zeabus_modbus_master.cpp
 *
 *  Created on: Jun 15, 2015
 *      Author: mahisorn
 */
#include <ros/ros.h>
#include <sensor_msgs/FluidPressure.h>
#include <geometry_msgs/TwistWithCovarianceStamped.h>


#include <modbus_ascii_ros/SendCommand.h>
#include <modbus_ascii_ros/Switch.h>
#include <modbus_ascii_ros/Pwm.h>

#include <mblib/MBDevice.h>
#include <termios.h>

#include <boost/thread.hpp>

#define REG_MAIN_ADDR 0x10
#define REG_AUX_ADDR  0x11
#define GPIO_ADDR     0x12
#define DRIVER_ADDR   0x30
#define ON 1
#define OFF 0

MBSlave* mb_slave_regulator_aux;
MBSlave* mb_slave_regulator_main;
MBSlave* mb_slave_gpio;
MBSlave* mb_slave_driver;

boost::mutex g_mutex;
ros::Publisher g_pub_baro;
ros::Publisher g_pub_baro_twist;
ros::Publisher g_pub_switch;
ros::Subscriber g_pwm_sub;

struct reg_aux_control_t
{
  union
  {
    struct
    {
      uint16_t pwr_m0 :1;
      uint16_t pwr_m1 :1;
      uint16_t pwr_m2 :1;
      uint16_t pwr_m3 :1;
      uint16_t pwr_m4 :1;
      uint16_t pwr_m5 :1;
      uint16_t pwr_m6 :1;
      uint16_t pwr_m7 :1;
      uint16_t pwr_pneumatic :1;
      uint16_t pwr_spare :1;
      uint16_t pwr_main :1;
      uint16_t pwr_gpio :1;
      uint16_t pwr_t_logic :1;
      uint16_t pwr_lamp1 :1;
      uint16_t pwr_lamp2 :1;
      uint16_t unused :1;
    };
  };
};
//reg_aux_control_t reg_aux_control;

bool setRegulatorAux(modbus_ascii_ros::SendCommand::Request  &req,
                     modbus_ascii_ros::SendCommand::Response &res)
{
  boost::mutex::scoped_lock(g_mutex);
  ROS_INFO_STREAM(req);

  uint16_t register_data = 0;
  for(size_t i = 0; (i < req.command.size()) && (i < 16); i++)
  {
    if(req.command[i])
    {
      register_data += (1 << i);
    }
  }


  int ret = mb_slave_regulator_aux->Write_multiple_registers(0, 1, &register_data);

  res.result = (ret == 1) ? true : false;
  return res.result;
}

bool setRegulatorMain(modbus_ascii_ros::SendCommand::Request  &req,
                      modbus_ascii_ros::SendCommand::Response &res)
{
  boost::mutex::scoped_lock(g_mutex);
  ROS_INFO_STREAM(req);

  uint16_t register_data = 0;
  for(size_t i = 0; (i < req.command.size()) && (i < 16); i++)
  {
    if(req.command[i])
    {
      register_data += (1 << i);
    }
  }

  int ret = mb_slave_regulator_main->Write_multiple_registers(0, 1, &register_data);

  res.result = (ret == 1) ? true : false;
  return res.result;
}

bool setGPIO(modbus_ascii_ros::SendCommand::Request  &req,
             modbus_ascii_ros::SendCommand::Response &res)
{
  boost::mutex::scoped_lock(g_mutex);
  ROS_INFO_STREAM(req);

  uint16_t register_data = 0;
  for(size_t i = 0; (i < req.command.size()) && (i < 16); i++)
  {
    if(req.command[i])
    {
      register_data += (1 << i);
    }
  }

  int ret = mb_slave_gpio->Write_multiple_registers(0, 1, &register_data);

  res.result = (ret == 1) ? true : false;
  return res.result;
}

void pwmCommandCallBack(const modbus_ascii_ros::PwmConstPtr& message)
{
  boost::mutex::scoped_lock(g_mutex);
  //ROS_INFO_STREAM(message);

  uint16_t pwm[8];
  for(int i = 0; i < 8; i++)
  {
    pwm[i] = message->pwm[i];
  }

  int ret = mb_slave_driver->Write_multiple_registers(0, 8, pwm);
  if(ret != 8)
  {
    ROS_ERROR_THROTTLE_NAMED(1.0, "zeabus_modbus_master", "PWM command failed");
  }
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "zeabus_modbus_master");
  ros::NodeHandle nh("~");

  std::string device;
  int baudrate;
  std::string frame_id;

  //nh.param<std::string>("device", device, "/dev/usb2serial/ftdi_AJ0390DC");
  nh.param<std::string>("device", device, "/dev/ttyUSB0");
  nh.param<std::string>("frame_id", frame_id, "baro_link");

  int fd = 0;


  if(Open_port(&fd, device.c_str()) != 0)
  {
    ROS_FATAL("Cannot open: %s", device.c_str());    
    return -1;
  }


  mb_slave_regulator_main = new MBSlave(REG_MAIN_ADDR, device.c_str(), "", B1000000, 0, 7, 2);
  mb_slave_regulator_aux = new MBSlave(REG_AUX_ADDR, device.c_str(), "", B1000000, 0, 7, 2);
  mb_slave_gpio = new MBSlave(GPIO_ADDR, device.c_str(), "", B1000000, 0, 7, 2);
  mb_slave_driver = new MBSlave(DRIVER_ADDR, device.c_str(), "", B1000000, 0, 7, 2);

  mb_slave_regulator_main->Init(fd);
  mb_slave_regulator_aux->Init(fd);
  mb_slave_gpio->Init(fd);
  mb_slave_driver->Init(fd);

  ros::ServiceServer service_set_aux = nh.advertiseService("set_regulator_aux", setRegulatorAux);
  ros::ServiceServer service_set_main = nh.advertiseService("set_regulator_main", setRegulatorMain);
  ros::ServiceServer service_set_gpio = nh.advertiseService("set_gpio", setGPIO);

  g_pub_baro = nh.advertise<sensor_msgs::FluidPressure>("/baro/pressure", 10);
  g_pub_baro_twist = nh.advertise<geometry_msgs::TwistWithCovarianceStamped>("/baro/data", 10);
  g_pub_switch = nh.advertise<modbus_ascii_ros::Switch>("/switch/data", 10);

  g_pwm_sub = nh.subscribe<modbus_ascii_ros::Pwm>("/pwm", 10, pwmCommandCallBack);

  ros::Rate rate(100);

  sensor_msgs::FluidPressure pressure;
  double last_depth = 0;
  geometry_msgs::TwistWithCovarianceStamped twist;
  pressure.header.frame_id = frame_id;
  twist.header.frame_id = frame_id;

  modbus_ascii_ros::Switch switch_data;


  while(ros::ok())
  {
    uint16_t baro;
    uint8_t switches;
    {
      boost::mutex::scoped_lock(g_mutex);
      //read baro

      mb_slave_gpio->Read_input_registers(0, 1, &baro);

      //read switch

      mb_slave_gpio->Read_discrete_inputs(0, 3, &switches);
    }

    //ROS_INFO("%d 0x%02x", baro, switches);

    //publish baro as z twist
    twist.header.stamp = ros::Time::now();

    /*
     * pressure sensor
     * Mesurement range 0 - 15 psi (gaged psi)
     * 0.5 - 4.5V
     * A2D 12 bits
     * 1.4579 psi/m
     */
    double psi = (((baro / 4096.0) * 5.0) / (4.5-0.5)) * 15.0;
    pressure.fluid_pressure = 6894.75729 * psi; //Pa
    double depth = psi * 1.4579; // m
    double dz = depth - last_depth;
    last_depth = depth;
    twist.twist.twist.linear.z = dz / rate.cycleTime().toSec();

    if(g_pub_baro.getNumSubscribers())
      g_pub_baro.publish(pressure);
    if(g_pub_baro_twist)
      g_pub_baro_twist.publish(twist);


    //publish switch
    switch_data.header.stamp = twist.header.stamp;

    switch_data.auto_switch = switches & 0x01;
    switch_data.motor_switch = switches & 0x02;
    switch_data.system_switch = switches & 0x03;

    if(g_pub_switch.getNumSubscribers())
      g_pub_switch.publish(switch_data);


    rate.sleep();
    ros::spinOnce();
  }






  return 0;
}

